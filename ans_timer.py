import threading
from random import randint

count = 0
print("정답을 입력해주세요.")
number = int(20)

def start_timer(count):
    count = count +1
    print(count,"초")
    timer=threading.Timer(1, start_timer, args=[count])
    timer.start()

    if count==int(number) : #입력한 값이 맞으면 멈추는 함수
        print("제한시간을 초과하였습니다", "폭탄이 터졌습니다",sep='\n', )
        timer.cancel()

start_timer(0)