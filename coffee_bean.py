# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter


SLACK_TOKEN = "xoxb-689627820624-689643358133-2thJS4u5Ty119eo3kb9BDfTw"
SLACK_SIGNING_SECRET = "336aab71249ad5887e5d8d98a8db8310"


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


def coffeebean_chart(text):
   if not "coffee" in text:
       return "`@<봇이름> coffee` 과 같이 멘션해주세요."

   # 여기에 함수를 구현해봅시다.
   url = "http://www.coffeebeankorea.com/menu/list.asp?category=18"
   req=urllib.request.Request(url)
   source_code = urllib.request.urlopen(url).read()
   soup = BeautifulSoup(source_code, "html.parser")

   # 여기에 함수를 구현해봅시다.
   coffee_list=['커피빈 coffee 메뉴']
   for i in soup.find("ul", class_="menu_list").find_all("span", class_="kor"):
       coffee_title = i.get_text().strip()
       coffee_list.append(coffee_title)

   return u'\n'.join(coffee_list)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
   channel = event_data["event"]["channel"]
   text = event_data["event"]["text"]

   message = coffeebean_chart(text)
   slack_web_client.chat_postMessage(
       channel=channel,
       text=message
   )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
   return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
   app.run('127.0.0.1', port=5000)