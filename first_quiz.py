import urllib.request

from bs4 import BeautifulSoup
from urllib import parse
from flask import Flask
from slack import WebClient
import requests
import threading
from slackeventsapi import SlackEventAdapter

SLACK_TOKEN = "xoxb-689627820624-678245088979-bCNxpwC3QcEQ1YFDVBjYgXK5"
SLACK_SIGNING_SECRET = "9a4f7864696d244a961d6d7a3d98d855"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

def start_timer(time):
    time = time -1
    if time <= 5 :
        print(time)
    timer= threading.Timer(1, start_timer, args=[time])
    timer.start()

    if time == 0 : #입력한 값이 맞으면 멈추는 함수
        print("제한시간을 초과하였습니다..." )
        timer.cancel()

# 크롤링 함수 구현하기
def _crawl_portal_keywords(text):

    new_text = text.split()[1]
    user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
    hdr = {'User-Agent': user_agent }

    url1 = 'https://namu.wiki/w/'
    word1 = parse.quote('가수')
    word2 = parse.quote('한국')
    url = url1 + word1 + '/' + word2
    #
    # req = urllib.request.Request(url=url , headers=hdr)
    source_code = requests.get(url, headers=hdr).text
    soup = BeautifulSoup(source_code, "html.parser")
    name_lists = []

    for i in soup.find_all("a", class_="wiki-link-internal"):
        name = i.get_text().strip()
        name_lists.append(name)

    if new_text in name_lists:
        yes = "정답"
        return yes
    else:
        no = "땡"
        return no


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    new_text = text.split()[1]

    if new_text == "시작":
        global count
        count = 0
        global correct_count
        correct_count = 0
        slack_web_client.chat_postMessage(
            channel=channel,
            text="20초 내에 가수 이름 다섯개를 입력해주세요." + "\n" + "-----------카운트 시작-----------"
        )
        start_timer(20)


    else:
        if count < 6:
            message = _crawl_portal_keywords(text)
            if message == "땡" :
                count = count + 1
                slack_web_client.chat_postMessage(
                    channel=channel,
                    text=message
                )
                if count == 5:
                    slack_web_client.chat_postMessage(
                        channel=channel,
                        text="%d 개 성공!" % correct_count
                    )
            else:
                count = count + 1
                correct_count = correct_count + 1
                slack_web_client.chat_postMessage(
                    channel=channel,
                    text=message
                )
                if count==5:
                    slack_web_client.chat_postMessage(
                        channel=channel,
                        text="%d 개 성공!" % correct_count
                    )




# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=5000)
